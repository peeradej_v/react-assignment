import React, { Component } from 'react';
import Button from './Button.js';

class ButtonList extends Component{

    constructor(props){
        super(props)
        this.state = props
    }

    buttonList(){
        return this.state.data.map((e,i) => {
            if(i === 0){
                return (
                    <div className="menu-button" key={i}>
                        <Button id={e.id} logo={e.logo} title={e.title} detail={e.detail}/>
                    </div>
                )
            }
            return (
                <div className="menu-button line-top" key={i}>
                    <Button id={e.id} logo={e.logo} title={e.title} detail={e.detail}/>
                </div>
            )
        })
    }

    render(){
        return (
             <div>
                {this.buttonList()}
             </div>
        )
    }
}

export default ButtonList