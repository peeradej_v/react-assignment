import React, { Component } from 'react';

class ButtonList extends Component{

    constructor(props){
        super(props)
        this.state = props
    }

    getDetail(element){

        try {

            const previeDetail = document.getElementsByClassName('menu-button-detail');
            for (let item of previeDetail) {
                item.classList.add('detail-none');
            }

            const previewId = parseInt(element.target.getAttribute("data-menu-id"));
            previeDetail[previewId].classList.remove('detail-none');

            const previeLogo = document.getElementsByClassName('menu-button-logo');
            for (let item of previeLogo) {
                item.classList.remove('logo-active');
                item.parentElement.classList.remove('button-active')
            }
            previeLogo[previewId].classList.add('logo-active');
            previeLogo[previewId].parentElement.classList.add('button-active')

            var nextId = previewId + 1;
            if(nextId >= previeDetail.length){
                nextId = 0;
            }
    
            const previewLists = document.getElementsByClassName('preview-image');
            for (let item of previewLists) {
                if(item.className == "preview-image show"){
                    item.className = "preview-image previous";
                }else{
                    item.className = "preview-image";
                }
            }

            previewLists[previewId].className = "preview-image show";
            previewLists[nextId].className = "preview-image next";

        }catch(err) {

        }

    }


    render(){
        return (
            <button className="" data-menu-id={this.state.id} onClick={(element) => this.getDetail(element)}>
                    <div data-menu-id={this.state.id} className="menu-button-logo logo-menu">
                        <img data-menu-id={this.state.id} src={this.state.logo} />
                    </div>
                    <div data-menu-id={this.state.id} className="menu-button-header">{this.state.title}</div>
                    <div data-menu-id={this.state.id} className="menu-button-detail detail-none">
                        {this.state.detail}
                    </div>
            </button>
        )
    }
}

export default ButtonList