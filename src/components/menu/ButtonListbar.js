import React, { Component } from 'react';

class ButtonListbar extends Component{

    constructor(props){
        super(props)
        this.state = { arr: props , loop : true , count: 0 }
        this.timerID = setInterval(() => this.tick(),2000)
    }

    componentDidMount(){
        try {
            const previewHeader = document.getElementsByClassName('trigger-button');
            previewHeader[0].click();
        }catch(err) {

        }
    }

    tick(){
        if(this.state.loop){
            if(this.state.count >= this.state.arr.data.length){
                this.setState({
                    count: 0
                })
            }
    
            const previewHeader = document.getElementsByClassName('trigger-button');
            previewHeader[this.state.count].click();
            
            this.setState({
                count: this.state.count + 1
            })
        }
    }
    
    trigger(element){

        const id = element.target.getAttribute("data-menu-id");
        document.getElementsByClassName('menu-button-header')[id].click();
        const trigger = document.getElementsByClassName('trigger-button')
        for (let item of trigger) {
            item.classList.remove('trigger-active');
        }
        trigger[id].classList.add('trigger-active');
    }


    buttonList(){
        return this.state.arr.data.map((e,i) => {
            return (
                <span data-menu-id={e.id} key={i} onClick={this.trigger} className="trigger-button"></span>
            )
        })
    }

    render(){

        return (
            <div className="button-list">
                {this.buttonList()}
            </div>
        )
    }
}

export default ButtonListbar