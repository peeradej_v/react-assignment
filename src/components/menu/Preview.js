import React, { Component } from 'react';

class Preview extends Component{

    constructor(props){
        super(props)
        this.state = props
    }

    // componentDidMount(){
    //     const previewLists = document.getElementsByClassName('preview-image');
    //     for(let i = 0; i < previewLists.length; i++){
    //         if(i === 0){
    //             previewLists[i].classList.add('show');
    //         }else if(i === 1){
    //             previewLists[i].classList.add('next');
    //         }
    //     }
    // }

    previewList(){
        return this.state.data.map((e,i) => {
            return <div className="preview-image" key={i}><img src={e.image}/></div>
        })
    }

    render(){
        return (
             <div className="preview-box">{this.previewList()}</div>
        )
    }
}

export default Preview