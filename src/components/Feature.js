import React, { Component } from 'react';
import '../style/Feature.css';

class Feature extends Component{

    render(){
        return (
            <div className="container">
                <div className="row">
                    <div className="feature-title">
                        <h1>100% enterprise ready</h1>
                        <div>Taskworld works like a charm in even highly requlated industries.</div>
                    </div>
                    <div className="col-12">
                        <div className="row">
                            <div className="col-6 col-lg-2 feature">
                                <div className="feature-circle">
                                    <div className="img-feature01"></div>
                                </div>
                                <div className="feature-detail">
                                    SAML 2.0 SSO
                                </div>
                            </div>
                            <div className="col-6 col-lg-2 feature">
                                <div className="feature-circle">
                                    <div className="img-feature02"></div>
                                </div>
                                <div className="feature-detail">
                                    VPC/On premise options
                                </div>
                            </div>
                            <div className="col-6 col-lg-2 feature">
                                <div className="feature-circle">
                                    <div className="img-feature03"></div>
                                </div>
                                <div className="feature-detail">
                                    two factor authentication
                                </div>
                            </div>
                            <div className="col-6 col-lg-2 feature">
                                <div className="feature-circle">
                                    <div className="img-feature04"></div>
                                </div>
                                <div className="feature-detail">
                                    Dedicated account manager
                                </div>
                            </div>
                            <div className="col-6 col-lg-2 feature">
                                <div className="feature-circle">
                                    <div className="img-feature05"></div>
                                </div>
                                <div className="feature-detail">
                                    SLA (Service Lavel Agreements)
                                </div>
                            </div>
                            <div className="col-6 col-lg-2 feature">
                                <div className="feature-circle">
                                    <div className="img-feature06"></div>
                                </div>
                                <div className="feature-detail">
                                    Privacy Shield certified
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="feature-more">
                        <button>Learn more ></button>
                    </div>
                </div>
            </div>
        )
    }
}

export default Feature