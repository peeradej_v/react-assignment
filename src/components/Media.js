import React, { Component } from 'react';
import '../style/Media.css';
import VDO from './media/vdo.js';

class Media extends Component{

    constructor(props){
        super(props)
        this.state = { vdo : '' }
        this.setVdo = this.setVdo.bind(this);
        this.escFunction = this.escFunction.bind(this);
    }

    setVdo(){
        this.setState({ vdo : <VDO /> })
    }

    escFunction(){
        this.setState({ vdo : '' })
    }
    
    componentDidMount(){
        document.addEventListener("keydown", this.escFunction, false);
    }
    componentWillUnmount(){
        document.removeEventListener("keydown", this.escFunction, false);
    }

    render(){

        return (
            <div className="container">
                <div id="vdo">
                    {this.state.vdo}
                </div>
                <div className="row media">
                    <div className="col-12 d-md-none">
                        <div className="vdo-button">
                            <img onClick={this.setVdo} src="image/highlight-image.png" />
                        </div>
                    </div>
                    <div className="col-12 col-md-4 col-lg-6">
                        <div className="row">
                            <div className="media-header-1">Ridiculously easy</div>
                            <div className="media-header-2">project management</div>
                            <div className="col-12 col-lg-6">
                                <div className="row">
                                    <button className="media-header-button button-try">try 15 day free</button>
                                </div>
                            </div>
                            <div className="col-12 col-lg-6">
                                <div className="row">
                                    <button className="media-header-button">Request demo</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="d-none d-md-block col-md-8 col-lg-6">
                        <div className="vdo-button">
                            <img onClick={this.setVdo} src="image/highlight-image.png" />
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row ">
                            <div className="col-12 partner-title">Trusted by leading companies worldwide</div>
                            <div className="col-6 col-lg-2">
                                <div className="partner logo-samsung"></div>
                            </div>
                            <div className="col-6 col-lg-2">
                                <div className="partner logo-amazon"></div>
                            </div>
                            <div className="col-6 col-lg-2">
                                <div className="partner logo-netflix"></div>
                            </div>
                            <div className="col-6 col-lg-2">
                                <div className="partner logo-salesforce"></div>
                            </div>
                            <div className="col-6 col-lg-2">
                                <div className="partner logo-roche"></div>
                            </div>
                            <div className="col-6 col-lg-2">
                                <div className="partner logo-att"></div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        )
    }
}

export default Media