import React, { Component } from 'react';
import '../style/Header.css';

class Header extends Component{

    render(){

        window.addEventListener('scroll', () => {
            if(window.scrollY > 0){
                document.getElementById('header').classList.add('header')
            }else{
                document.getElementById('header').classList.remove('header')
            }
        })

        return (
            <div className="row">
                <div id="header" className="col-12 fixed-top">
                    <div className="row">
                        <div className="col-3 col-sm-1 d-lg-none">
                        
                        </div>
                        <div className="col-2 col-sm-3 col-lg-2">
                            <div className="logo-box">
                                <div className="logo-short"></div>
                            </div>
                        </div>
                        <div className="d-none d-lg-block col-lg-6 col-xl-5 menu-header">
                            <span className="menu-header-button">Product</span>
                            <span className="menu-header-button">Pricing</span>
                            <span className="menu-header-button">Success</span>
                            <span className="menu-header-button">Enterprise</span>
                            <span className="menu-header-button">Contact us</span>
                        </div>
                        <div className="d-none d-xl-block col-xl-2 menu-telephone">
                            <div className="telephone-number"><img src="/image/fill-1-copy.png"/>+1-866-428-9571</div>
                        </div>
                        <div className="d-none d-lg-block offset-lg-1 col-lg-1 offset-xl-0">
                            <button className="logo-login-header">log in</button>
                        </div>
                        <div className="col-7 offset-sm-4 col-sm-4 offset-lg-0 col-lg-2 col-xl">
                            <button className="logo-try-header">Try 15 days free</button>
                        </div>                     
                    </div>
                </div>
            </div>
        )
    }
}

export default Header