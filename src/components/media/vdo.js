import React, { Component } from 'react';

class VDO extends Component{

    render(){
        return (
            <div className="vdo-box">
                <iframe width="1280px" height="720px" src="https://www.youtube.com/embed/jZe8VQ63iXQ?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="true"></iframe>
            </div>
        )
    }
}

export default VDO