import React, { Component } from 'react';
import '../style/Menu.css';
import Preview from './menu/Preview.js';
import ButtonList from './menu/ButtonList.js';
import ButtonListbar from './menu/ButtonListbar.js';

class Menu extends Component{

    render(){

        const list = [
            { id: 0, image: "/image/localization-01-3-x.png", logo: '/image/project-management-icon.png', title: "Project management", detail: "Effortlessly assign and track work with visual boards." },
            { id: 1, image: "/image/localization-02-3-x.png", logo: '/image/time-tracking-icon.png', title: "Team messaging", detail: "See who's online, who has read your message and receive instant response." },
            { id: 2, image: "/image/localization-01-3-x.png", logo: '/image/file-management-icon.png', title: "Time tracking", detail: "Track time spent on task and get logs for each project." },
            { id: 3, image: "/image/localization-01-3-x.png", logo: '/image/team-messaging-icon.png', title: "File management", detail: "Each file is where it's supposed to be, attached to the relevant task and project." },
            { id: 4, image: "/image/localization-02-3-x.png", logo: '/image/performance-reports-icon.png', title: "Preformance reports ", detail: "Stay on top of your performance with reports and real-time feedback." },
            { id: 5, image: "/image/localization-01-3-x.png", logo: '/image/gantt-chart-timeline-icon.png', title: "Gantt chart", detail: "Visualize project progress in a timeline and anticipate potential problems." }
        ];

        return (
            <div className="container-fluid menu-background">
                <div className="row">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="menu-title">
                                    <h2>Thousands of companies in every imaginable industry track work, measure performance and deliver great results with Taskworld.</h2>
                                </div>
                            </div>
                            <div className="offset-2 col-10 offset-md-1 col-md-11 offset-lg-0 col-lg-8">
                                <Preview data={list} />
                            </div>
                            <div className="col-lg-4">
                                <ButtonList data={list} />
                            </div>
                            <div className="col-12">
                                <ButtonListbar data={list} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Menu