import React, { Component } from 'react';
import '../style/App.css';
import Header from './Header';
import Footer from './Footer.js';
import Award from './Award.js';
import Share from './Share.js';
import Feature from './Feature.js';
import Menu from './Menu.js';
import Media from './Media.js';
import VDO from './media/vdo.js';

class App extends Component {

  render() {

    return (
      <div className="row">
        <div className="container-fluid">
          <Header />
          <Media />
          <Menu />
          <Feature />
          <Share />
          <Award />
          <Footer />
        </div>
      </div>
    );
  }
}

export default App;
