import React, { Component } from 'react';
import '../style/Award.css';

class Award extends Component{

    render(){
        return (
            <div className="container-fuild award-background">
                <div className="row">
                    <div className="container">
                        <div className="row">
                            <div className="award"><img src="image/getapp-project-management-category-leader.png"/></div>
                            <div className="award"><img src="image/testimonial-01.png"/></div>
                            <div className="award"><img src="image/testimonial-02.png"/></div>
                            <div className="award"><img src="image/testimonial-03.png"/></div>
                            <div className="award"><img src="image/testimonial-04.png"/></div>
                            <div className="award"><img src="image/testimonial-05.png"/></div>
                            <div className="award"><img src="image/testimonial-06.png"/></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Award