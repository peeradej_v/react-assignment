import React, { Component } from 'react';
import '../style/Footer.css';

class Footer extends Component{

    render(){
        return (
            <div className="container footer">
                <div className="col-12">
                    <div className="row">
                        <div className="col-12 col-lg-3 footer-menu">
                            <div className="footer-menu-title">Tankworld Inc.</div>
                            <div>
                                <div className="footer-menu-detail">28 Valley Road, Suite#1 Montclair, NJ 07042</div>
                            </div>
                            <div className="padding-top">
                                <div className="footer-menu-detail"><span className=""></span> +(1) 866 428 9571</div>
                                <div className="footer-menu-detail"><span className=""></span> support@taskworld.com</div>
                            </div>
                        </div>
                        <div className="col-12 col-lg-2 footer-menu">
                            <div className="footer-menu-title">Solutions</div>
                            <div className="footer-menu-detail">Product</div>
                            <div className="footer-menu-detail">Enterprise</div>
                            <div className="footer-menu-detail">Pricing</div>
                            <div className="footer-menu-detail">Success Stories</div>
                        </div>
                        <div className="col-12 col-lg-2 footer-menu">
                            <div className="footer-menu-title">Company</div>
                            <div className="footer-menu-detail">About us</div>
                            <div className="footer-menu-detail">Careers</div>
                            <div className="footer-menu-detail">Press & media</div>
                            <div className="footer-menu-detail">Blog</div>
                            <div className="footer-menu-detail">Contant us</div>
                            <div className="footer-menu-detail">Term, privacy & security</div>
                        </div>
                        <div className="col-12 col-lg-2 footer-menu">
                            <div className="footer-menu-title">Resources</div>
                            <div className="footer-menu-detail">User guide</div>
                            <div className="footer-menu-detail">Getting started</div>
                            <div className="footer-menu-detail">Taskworld basixs</div>
                            <div className="footer-menu-detail">Youtube channel</div>
                            <div className="footer-menu-detail">Use cases</div>                       
                        </div>
                        <div className="col-12 col-lg-3 footer-menu">
                            <div className="footer-menu-title">Choose Your Lanuage</div>

                            <div className="footer-menu-title">Mobile Apps</div>
                            <div className="footer-menu-detail"><img src="image/app-store-badge.png"/></div>
                            <div className="footer-menu-detail"><img src="image/google-play-badge.png"/></div>
                        </div>
                    </div>
                    <hr/>
                    <div className="row">
                        <div className="col-8">
                            <div className="row">
                                <div>Copyright © 2013-2017™. Pat. Pend. All rights reserved. Terms, Privacy & Security</div>
                            </div>
                        </div>
                        <div className="col-4">
                            <div className="row">
                                <span className="logo-facebook"></span>
                                <span className="logo-twitter"></span>
                                <span className="logo-google"></span>
                                <span className="logo-linkedin"></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default Footer