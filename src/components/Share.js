import React, { Component } from 'react';
import '../style/Share.css';

class Share extends Component{

    render(){
        return (
            <div className="container-fuild">
                <div className="row">
                    <div className="share-device"></div>
                    <div className="share-background">
                        <div className="container">
                            <div className="share-title">
                                <h1>Still on the fence? talk with us, share your requirements and see if Taskworld can help.</h1>
                            </div>
                            <div className="share-button">
                                <button>Request demo</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Share